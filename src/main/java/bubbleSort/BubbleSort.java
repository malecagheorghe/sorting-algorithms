package bubbleSort;

import generate.SortingAlgorithm;

public class BubbleSort implements SortingAlgorithm {

    public static long bubbleSort(int[] unsortedArray) {
        long numberOfComparisons = 0;
        long numberOfAssignments = 0;
        int thirdBucket;
        for (int i = unsortedArray.length; i > 0; i--) {
            numberOfComparisons++;
            numberOfAssignments++;
            for (int j = 1; j < i; j++) {
                numberOfComparisons++;
                numberOfAssignments++;
                if (unsortedArray[j - 1] > unsortedArray[j]) {
                    numberOfComparisons++;
                    thirdBucket = unsortedArray[j - 1];
                    unsortedArray[j - 1] = unsortedArray[j];
                    unsortedArray[j] = thirdBucket;
                    numberOfAssignments += 3;
                }
            }
        }
        return (numberOfAssignments + numberOfComparisons);
    }

    @Override
    public String toString() {
        return "BubbleSort";
    }

    @Override
    public long sort(int[] unsortedArray) {
        return bubbleSort(unsortedArray);
    }
}
