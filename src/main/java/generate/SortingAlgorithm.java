package generate;

public interface SortingAlgorithm {
    long sort(int[] unsortedArray);
}
