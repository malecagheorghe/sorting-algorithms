package generate;

import java.util.Arrays;
import java.util.Random;

public class RandomArray {
    public static int[] randomIntArray(int arrayLength) {
        int[] randomArray = new int[arrayLength];
        Random random = new Random();
        for (int i = 0; i < arrayLength; i++) {
            randomArray[i] = random.nextInt(UsefulVariables.MAX_RANGE);
        }
        return (randomArray);
    }

    public static int[] randomIntArrayAscending(int arrayLength) {
        int[] randomArray = randomIntArray(arrayLength);
        Arrays.sort(randomArray);
        return randomArray;
    }

    public static int[] randomIntArrayDescending(int arrayLength) {
        int[] randomArray = randomIntArrayAscending(arrayLength);
        int[] temporaryArray = new int[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            temporaryArray[i] = randomArray[arrayLength - 1 - i];
        }
        return (temporaryArray);
    }
}
