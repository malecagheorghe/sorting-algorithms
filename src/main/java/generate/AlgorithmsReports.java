package generate;

import bubbleSort.BubbleSort;
import mergeSort.MergeSort;
import quickSort.QuickSort;

import java.io.IOException;

public class AlgorithmsReports {
    public static void main(String[] args) throws IOException {
        System.out.println("START merge sort");
        new AlgorithmPerformance("src/main/java/files/merge-sort-performance.xlsx").generateReport(Scenario.all, new MergeSort());
        System.out.println("FINISHED merge sort");
        System.out.println("START quick sort");
        new AlgorithmPerformance("src/main/java/files/quick-sort-performance.xlsx").generateReport(Scenario.all,new QuickSort());
        System.out.println("FINISHED quick sort");
        System.out.println("START bubble sort");
        new AlgorithmPerformance("src/main/java/files/bubble-sort-performance.xlsx").generateReport(Scenario.all,new BubbleSort());
        System.out.println("FINISHED bubble sort");
        System.out.println("START best case sort");
        new AlgorithmPerformance("src/main/java/files/best-case-performance.xlsx").generateReport(Scenario.best,new BubbleSort(), new QuickSort(), new MergeSort());
        System.out.println("FINISHED best case sort");
        System.out.println("START average case sort");
        new AlgorithmPerformance("src/main/java/files/average-case-performance.xlsx").generateReport(Scenario.average,new BubbleSort(), new QuickSort(), new MergeSort());
        System.out.println("FINISHED average case sort");
        System.out.println("START worst case sort");
        new AlgorithmPerformance("src/main/java/files/worst-case-performance.xlsx").generateReport(Scenario.worst,new BubbleSort(), new QuickSort(), new MergeSort());
        System.out.println("FINISHED worst case sort");
    }
}