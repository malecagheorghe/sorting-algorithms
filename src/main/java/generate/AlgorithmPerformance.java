package generate;

import excel.Xcell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import java.io.IOException;

import static generate.RandomArray.*;
import static generate.UsefulVariables.*;

public class AlgorithmPerformance extends Xcell {

    public AlgorithmPerformance(String filePath) {
        super(filePath);
    }

    public void generateReport(Scenario scenario, SortingAlgorithm... sortingAlgorithms) throws IOException {
        Sheet sheet;
        if (sortingAlgorithms.length == 1) {
            sheet = createTable("mergeSort", "Array Length", "Ascending Order", "Random Order", "Descending Order");
        } else {
            sheet = createTable(scenario.toString(), "Array Length", sortingAlgorithms[0].toString(), sortingAlgorithms[1].toString(), sortingAlgorithms[2].toString());
        }

        for (int arrayLength = 100, rowIndex = 1; arrayLength < AMOUNT_OF_ITERATIONS; arrayLength += ITERATION_STEP, rowIndex++) {
            Row row = sheet.createRow(rowIndex);
            createCell(row, 0, arrayLength);
            if (sortingAlgorithms.length == 1) createCells(sortingAlgorithms[0], arrayLength, row);
            else {
                createCells(sortingAlgorithms, scenario, arrayLength, row);
            }
        }

        saveFile(sheet.getWorkbook());
        closeWorkbook(sheet);
    }

    private void createCells(SortingAlgorithm sortingAlgorithm, int arrayLength, Row row) {
        createCell(row, 1, sortingAlgorithm.sort(randomIntArrayAscending(arrayLength)));
        createCell(row, 2, getMeanValue(sortingAlgorithm, arrayLength));
        createCell(row, 3, sortingAlgorithm.sort(randomIntArrayDescending(arrayLength)));
    }

    private void createCells(SortingAlgorithm[] sortingAlgorithms, Scenario scenario, int arrayLength, Row row) {
        for (int i = 0; i < 3; i++) {
            switch (scenario.toString()) {
                case "best":
                    createCell(row, i + 1, sortingAlgorithms[i].sort(randomIntArrayAscending(arrayLength)));
                    break;
                case "average":
                    createCell(row, i + 1, sortingAlgorithms[i].sort(randomIntArray(arrayLength)));
                    break;
                case "worst":
                    createCell(row, i + 1, sortingAlgorithms[i].sort(randomIntArrayDescending(arrayLength)));
                    break;
            }
        }
    }

    private static long getMeanValue(SortingAlgorithm sortingAlgorithm, int length) {
        long meanValue = 0;
        for (int j = 0; j < NUMBER_OF_AVERAGE_CASES; j++) {
            meanValue += sortingAlgorithm.sort(randomIntArray(length));
        }
        return meanValue / NUMBER_OF_AVERAGE_CASES;
    }
}
