package generate;

public class UsefulVariables {
    public static final int AMOUNT_OF_ITERATIONS = 10_000;
    public static final int ITERATION_STEP = 100;
    public static final int MAX_RANGE = 1_000;
    public static final int NUMBER_OF_AVERAGE_CASES = 20;
}
