package quickSort;

import generate.SortingAlgorithm;

public class QuickSort implements SortingAlgorithm {
    private static long numberOfAssignments;
    private static long numberOfComparisons;

    public static long quickSort(int[] unsortedArray) {
        numberOfAssignments = 0;
        numberOfComparisons = 0;
        quickSortRecursive(unsortedArray, 0, unsortedArray.length - 1);
        return (numberOfAssignments + numberOfComparisons);
    }

    private static void quickSortRecursive(int[] unsortedArray, int firstIndex, int lastIndex) {
        if (firstIndex < lastIndex) {
            numberOfComparisons++;
            int pivot = partition(unsortedArray, firstIndex, lastIndex);
            numberOfAssignments++;
            quickSortRecursive(unsortedArray, firstIndex, pivot - 1);
            quickSortRecursive(unsortedArray, pivot + 1, lastIndex);
        }
    }

    private static int partition(int[] myArray, int firstIndex, int lastIndex) {
        swapValues(myArray, lastIndex, ((lastIndex - firstIndex) / 2) + firstIndex);
        int lessThan = firstIndex - 1;
        numberOfAssignments++;
        for (int greaterThan = firstIndex; greaterThan < lastIndex; greaterThan++) {
            numberOfComparisons++;
            numberOfAssignments++;
            if (myArray[greaterThan] < myArray[lastIndex]) {
                numberOfComparisons++;
                lessThan++;
                numberOfAssignments++;
                swapValues(myArray, lessThan, greaterThan);
            }
        }
        swapValues(myArray, lessThan + 1, lastIndex);
        return lessThan + 1;
    }

    private static void swapValues(int[] myArray, int firstIndex, int secondIndex) {
        int z = myArray[firstIndex];
        myArray[firstIndex] = myArray[secondIndex];
        myArray[secondIndex] = z;
        numberOfAssignments += 3;
    }

    @Override
    public String toString() {
        return "QuickSort";
    }

    @Override
    public long sort(int[] unsortedArray) {
        return quickSort(unsortedArray);
    }
}
