package mergeSort;

import generate.SortingAlgorithm;

public class MergeSort implements SortingAlgorithm {
    private static long numberOfComparisons;
    private static long numberOfAssignments;

    private static void merge(int[] A, int l, int m, int r) {
        int nL = m - l + 1;
        int nR = r - m;
        numberOfAssignments += 2;
        int[] left = new int[nL];
        int[] right = new int[nR];
        numberOfAssignments += 2;

        for (int i = 0; i < nL; ++i) {
            numberOfAssignments++;
            numberOfComparisons++;
            left[i] = A[l + i];
            numberOfAssignments++;
        }

        for (int j = 0; j < nR; ++j) {
            numberOfComparisons++;
            numberOfAssignments++;
            right[j] = A[m + 1 + j];
            numberOfAssignments++;
        }
        int i = 0, j = 0;
        int k = l;
        numberOfAssignments += 3;

        while (i < nL && j < nR) {
            numberOfComparisons++;
            if (left[i] <= right[j]) {
                numberOfComparisons++;
                A[k] = left[i];
                i++;
                numberOfAssignments += 2;
            } else {
                A[k] = right[j];
                j++;
                numberOfAssignments += 2;
            }
            k++;
            numberOfAssignments++;
        }

        while (i < nL) {
            numberOfComparisons++;

            A[k] = left[i];
            i++;
            k++;
            numberOfAssignments += 3;

        }

        while (j < nR) {
            numberOfComparisons++;

            A[k] = right[j];
            j++;
            k++;
            numberOfAssignments += 3;
        }
    }

    private static void mergeSortRecursive(int[] A, int l, int r) {
        if (l < r) {
            numberOfComparisons++;
            int mid = (l + r) / 2;
            numberOfAssignments++;
            mergeSortRecursive(A, l, mid);
            mergeSortRecursive(A, mid + 1, r);
            merge(A, l, mid, r);
        }
    }

    public static long mergeSort(int[] unsortedArray) {
        numberOfAssignments = 0;
        numberOfComparisons = 0;
        mergeSortRecursive(unsortedArray, 0, unsortedArray.length - 1);
        return numberOfAssignments + numberOfComparisons;
    }

    @Override
    public String toString() {
        return "MergeSort";
    }

    @Override
    public long sort(int[] unsortedArray) {
        return mergeSort(unsortedArray);
    }
}
