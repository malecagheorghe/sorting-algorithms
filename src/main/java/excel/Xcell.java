package excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Xcell {
    private String filePath;
    private Workbook workbook = new XSSFWorkbook();

    public Xcell(String filePath) {
        this.filePath = filePath;
    }

    public Workbook readFile() throws IOException {
        FileInputStream file = new FileInputStream(new File(this.filePath));
        return new XSSFWorkbook(file);
    }

    public Sheet createTable(String name, String... fields) throws IOException {
        Sheet sheet = workbook.createSheet(name);
        createRow(sheet, 0, fields);
        return sheet;
    }

    public void createRow(Sheet sheet, int index, String... names) {
        Row row = sheet.createRow(index);
        for (int i = 0; i < names.length; i++) {
            createCell(row, i, names[i]);
        }
    }

    public Cell createCell(Row row, int cellIndex, String value) {
        Cell cell = row.createCell(cellIndex);
        cell.setCellValue(value);
        return cell;
    }

    public Cell createCell(Row row, int cellIndex, long value) {
        Cell cell = row.createCell(cellIndex);
        cell.setCellValue(value);
        return cell;
    }

    public void saveFile(Workbook workbook) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(this.filePath);
        workbook.write(outputStream);
        outputStream.close();
    }

    public Cell getCell(String sheetName, int rowIndex, int colIndex) throws IOException {
        return getRow(sheetName, rowIndex).getCell(colIndex);
    }

    public Row getRow(String sheetName, int rowIndex) throws IOException {
        return getSheet(sheetName).getRow(rowIndex);
    }

    public Sheet getSheet(String sheetName) throws IOException {
        FileInputStream file = new FileInputStream(new File(this.filePath));
        Workbook workbook = new XSSFWorkbook(file);
        return workbook.getSheet(sheetName);
    }

    public void closeWorkbook(Cell cell) throws IOException {
        closeWorkbook(cell.getSheet());
    }

    public void closeWorkbook(Row row) throws IOException {
        closeWorkbook(row.getSheet());
    }

    public void closeWorkbook(Sheet sheet) throws IOException {
        closeWorkbook(sheet.getWorkbook());
    }

    public void closeWorkbook(Workbook workbook) throws IOException {
        workbook.close();
    }
}
